package com.jigarnaik.securityservice.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Documented
@Constraint(validatedBy = SecurityValidator.class)
@Retention(RetentionPolicy.RUNTIME)
@ReportAsSingleViolation
public @interface ValidateSecurity {

    String message() default "Invalid Security";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
