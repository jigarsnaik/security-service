package com.jigarnaik.securityservice.validator;

import com.jigarnaik.securityservice.constants.Action;
import com.jigarnaik.securityservice.model.Security;
import com.jigarnaik.securityservice.repostiroy.SecurityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.function.BiFunction;

@Component
public class SecurityValidator implements ConstraintValidator<ValidateSecurity, Security> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SecurityValidator.class);

    @Autowired
    private SecurityRepository securityRepository;

    BiFunction<Security, ConstraintValidatorContext, Boolean> validateForDuplicate = (security, context) -> {
        boolean isValid = true;
        if (securityRepository.countByIsinOrName(security.getIsin(), security.getName()) != 0)
            isValid = addError("Security with ISIN or name already exist.", context, isValid);
        return isValid;
    };

    BiFunction<Security, ConstraintValidatorContext, Boolean> validateForExist = (security, context) -> {
        boolean isValid = true;
        Security secExample = new Security();
        secExample.setId(security.getId());
        if (securityRepository.count(Example.of(secExample)) == 0)
            isValid = addError("Security not found", context, isValid);
        return isValid;
    };

    @Override
    public boolean isValid(Security security, ConstraintValidatorContext context) {
        if (Action.CREATE.equals(security.getAction())) {
            return validateForDuplicate.apply(security, context);
        } else if (Action.UPDATE.equals(security.getAction()) || Action.DELETE.equals(security.getAction())) {
            return validateForExist.apply(security, context);
        }
        return true;
    }


    private boolean addError(String errorMessage, ConstraintValidatorContext context, Boolean isValid) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(errorMessage).addConstraintViolation();
        return !isValid && isValid;

    }

}
