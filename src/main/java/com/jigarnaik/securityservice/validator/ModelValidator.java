package com.jigarnaik.securityservice.validator;

import com.jigarnaik.securityservice.constants.Action;
import com.jigarnaik.securityservice.model.BaseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class ModelValidator<E> {

    @Autowired
    private Validator validator;

    public <E extends BaseEntity> void validate(E request, Action action) throws ValidationException {
        request.setAction(action);
        Set<ConstraintViolation<E>> violations = validator.validate(request);
        if (!violations.isEmpty()) {
            throw new ValidationException(violations.stream().map(ConstraintViolation::getMessage).sorted().collect(Collectors.joining(",")));
        }
    }
}