package com.jigarnaik.securityservice.service.exception;

public class SecurityNotFoundException extends RuntimeException {

    public SecurityNotFoundException(String e) {
        super(e);
    }
}
