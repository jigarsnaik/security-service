package com.jigarnaik.securityservice.service;

import com.jigarnaik.securityservice.model.Security;
import com.jigarnaik.securityservice.repostiroy.SecurityRepository;
import com.jigarnaik.securityservice.service.exception.SecurityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SecurityService {

    @Autowired
    SecurityRepository securityRepository;

    public Security save(Security security) {
        return securityRepository.save(security);
    }

    public Security findById(Long id) {
        return securityRepository.findById(id).orElseThrow(() -> new SecurityNotFoundException("Security not found matching id " + id));
    }

    public List<Security> findAll() {
        return securityRepository.findAll();
    }

    public void delete(Security security) {
        securityRepository.delete(security);
    }
}
