package com.jigarnaik.securityservice.repostiroy;

import com.jigarnaik.securityservice.model.Security;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SecurityRepository extends JpaRepository<Security, Long> {

    long countByIsinOrName(String isin, String name);
}
