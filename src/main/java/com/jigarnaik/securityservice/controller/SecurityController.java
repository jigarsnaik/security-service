package com.jigarnaik.securityservice.controller;

import com.jigarnaik.securityservice.constants.Action;
import com.jigarnaik.securityservice.model.Security;
import com.jigarnaik.securityservice.service.SecurityService;
import com.jigarnaik.securityservice.validator.ModelValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/security")
public class SecurityController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SecurityController.class);
    @Autowired
    SecurityService securityService;

    @Autowired
    private ModelValidator<Security> validator;

    @GetMapping
    public List<Security> list() {
        return securityService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Security> findById(@PathVariable(required = true, name = "id") Long id) {
        return ResponseEntity.ok(securityService.findById(id));
    }

    @PostMapping
    public ResponseEntity<Security> create(@RequestBody(required = true) Security security) {
        validator.validate(security, Action.CREATE);
        return ResponseEntity.ok(securityService.save(security));
    }

    @PutMapping
    public ResponseEntity<Security> update(@RequestBody(required = true) Security security) {
        validator.validate(security, Action.UPDATE);
        return ResponseEntity.ok(securityService.save(security));
    }

    @DeleteMapping
    public void delete(@RequestBody(required = true) Security security) {
        validator.validate(security, Action.DELETE);
        securityService.delete(security);
    }
}
