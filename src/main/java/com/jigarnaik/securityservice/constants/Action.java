package com.jigarnaik.securityservice.constants;

public enum Action {

    CREATE,
    UPDATE,
    DELETE
}
