package com.jigarnaik.securityservice.model;

import com.jigarnaik.securityservice.validator.ValidateSecurity;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@ValidateSecurity
@Entity
@Table(name = "SECURITY")
public class Security extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "ISIN is required")
    @Column(name = "ISIN")
    private String isin;

    @Column(name = "SECURITY_NAME")
    @NotBlank(message = "security name is required")
    private String name;

    public Security() {
    }

    public Security(String isin, String name) {
        this.isin = isin;
        this.name = name;
    }

    public static Security copy(Security security) {
        return new Security(security.getIsin(), security.getName());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIsin() {
        return isin;
    }

    public void setIsin(String isin) {
        this.isin = isin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
