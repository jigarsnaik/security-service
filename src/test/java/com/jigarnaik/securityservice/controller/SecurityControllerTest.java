package com.jigarnaik.securityservice.controller;

import com.jigarnaik.securityservice.constants.Action;
import com.jigarnaik.securityservice.model.Security;
import com.jigarnaik.securityservice.repostiroy.SecurityRepository;
import com.jigarnaik.securityservice.service.exception.SecurityNotFoundException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class SecurityControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private SecurityRepository repository;

    private String baseUrl = null;

    @BeforeEach
    public void beforeEach() {
        baseUrl = "http://localhost:" + port + "/";
    }

    @AfterEach
    public void afterEach() {
        repository.deleteAll();
    }

    @Test
    public void createSecurityTest() {
        Security security = new Security("AP1000100", "Apple inc");
        ResponseEntity<Security> securityResponseEntity = this.restTemplate.postForEntity(baseUrl + "security", security, Security.class);
        assertEquals(HttpStatus.OK, securityResponseEntity.getStatusCode());
        assertNotNull(securityResponseEntity.getBody());
        assertNotNull(securityResponseEntity.getBody().getId());
        assertNotNull(securityResponseEntity.getBody().getCreatedOn());
        assertNull(securityResponseEntity.getBody().getUpdatedOn());
        assertEquals(security.getIsin(), securityResponseEntity.getBody().getIsin());
        assertEquals(security.getName(), securityResponseEntity.getBody().getName());
    }

    @Test
    public void updateSecurityTest() {
        Security security = new Security("AP1000100", "Apple India");
        security.setAction(Action.CREATE);
        repository.save(security);
        Security security1 = new Security("AP1000100", "Apple Inc");
        security1.setAction(Action.UPDATE);
        security1.setId(security.getId());
        security1.setCreatedOn(security.getCreatedOn());
        RequestEntity<Security> requestEntity = new RequestEntity(security1, HttpMethod.PUT, URI.create(baseUrl + "security"));
        ResponseEntity<Security> securityResponseEntity = this.restTemplate.exchange(baseUrl + "security", HttpMethod.PUT, requestEntity, Security.class);
        assertEquals(HttpStatus.OK, securityResponseEntity.getStatusCode());
        assertNotNull(securityResponseEntity.getBody());
        assertNotNull(securityResponseEntity.getBody().getId());
        assertNotNull(securityResponseEntity.getBody().getCreatedOn());
        assertNotNull(securityResponseEntity.getBody().getUpdatedOn());
        assertEquals(security1.getId(), securityResponseEntity.getBody().getId());
        assertEquals(security1.getIsin(), securityResponseEntity.getBody().getIsin());
        assertEquals(security1.getName(), securityResponseEntity.getBody().getName());
    }

    @Test
    public void deleteSecurityTest() {
        Security security = new Security("AP1000100", "Apple India");
        security.setAction(Action.CREATE);
        security = repository.save(security);
        RequestEntity<Security> requestEntity = new RequestEntity(security, HttpMethod.PUT, URI.create(baseUrl + "security"));
        this.restTemplate.exchange(baseUrl + "security", HttpMethod.DELETE, requestEntity, Security.class);
        this.restTemplate.delete(baseUrl + "security", security);
        Optional<Security> securityOptional = repository.findById(security.getId());
        assertFalse(securityOptional.isPresent());
    }

    @Test
    public void findByIdTest() {
        Security security = new Security("AP1000100", "Apple India");
        security.setAction(Action.CREATE);
        security = repository.save(security);
        Map<String, Long> uriParams = new HashMap<>();
        uriParams.put("id", security.getId());
        ResponseEntity<Security> responseEntity = this.restTemplate.getForEntity(baseUrl + "security/{id}", Security.class, uriParams);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
    }

    @Test
    public void findByIdTest_throwsSecurityNotFoundException() {
        Map<String, Long> uriParams = new HashMap<>();
        uriParams.put("id", 1l);
        ResponseEntity<Security> responseEntity = this.restTemplate.getForEntity(baseUrl + "security/{id}", Security.class, uriParams);
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }

    @Test
    public void findAll() {
        Security securityApple = new Security("AP1000100", "Apple India");
        securityApple.setAction(Action.CREATE);
        Security securityRel = new Security("AP1000200", "Reliance India");
        securityRel.setAction(Action.CREATE);

        repository.save(securityApple);
        repository.save(securityRel);

        ResponseEntity<List> responseEntity = this.restTemplate.getForEntity(baseUrl + "security", List.class);
        assertEquals(2, responseEntity.getBody().size());
    }


}