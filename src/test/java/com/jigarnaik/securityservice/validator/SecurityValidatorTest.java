package com.jigarnaik.securityservice.validator;

import com.jigarnaik.securityservice.constants.Action;
import com.jigarnaik.securityservice.model.Security;
import com.jigarnaik.securityservice.repostiroy.SecurityRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import javax.validation.ConstraintValidatorContext;
import java.util.stream.Stream;

@SpringBootTest
@ActiveProfiles("test")
public class SecurityValidatorTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(SecurityValidatorTest.class);
    @Autowired
    private SecurityValidator validator;
    @Autowired
    private SecurityRepository repository;
    @MockBean
    private ConstraintValidatorContext context;
    @MockBean
    private ConstraintValidatorContext.ConstraintViolationBuilder builder;

    private static Stream<Arguments> testDataProvider() {
        return Stream.of(
                Arguments.of(createSecurityObject(null, "AP10100010", "Apple Inc", Action.CREATE), createSecurityObject(null, "AP10100010", "Apple Inc", Action.CREATE), false, "Security already exist"),
                Arguments.of(null, createSecurityObject(null, "AP10100010", "Apple Inc", Action.CREATE), true, "Security already exist"),
                Arguments.of(null, createSecurityObject(1l, "AP10100010", "Apple Inc", Action.UPDATE), false, "Update non existing security"),
                Arguments.of(createSecurityObject(null, "AP10100010", "Apple Inc", Action.CREATE), createSecurityObject(1l, "AP10100010", "Apple Inc", Action.UPDATE), true, "Update non existing security")
        );
    }

    private static Security createSecurityObject(Long id, String isin, String name, Action action) {
        Security security = new Security(isin, name);
        security.setAction(action);
        return security;
    }

    @BeforeEach
    public void beforeEach() {
        Mockito.when(context.buildConstraintViolationWithTemplate(Mockito.anyString())).thenReturn(builder);
    }

    @AfterEach
    public void afterEach() {
        repository.deleteAll();
    }


    @ParameterizedTest(name = "isValid - {3}")
    @MethodSource("testDataProvider")
    public void isValid(Security existingSecurity, Security testSecurity, Boolean expected, String testName) {
        if (existingSecurity != null) {
            repository.save(existingSecurity);
        }
        boolean isValid = validator.isValid(testSecurity, context);
        Assertions.assertEquals(expected, isValid);
    }

}