# Security Service

Required:
- MongoDB
- Maven 
- Java8

----------
Operations
----------
- Add New Security 
- Update Security 
- Delete Security 
- FindAll
- FindById

Launch below swagger URL to test the service.  
Swagger URL -> http://localhost:8081/swagger-ui.html

- Sample post request.
```
{
    "isin": "APP12122",
    "name": "Apple"
}
```